import tensorflow as tf
import numpy as np

import os
import time
import random
import glob

from layers import general_conv2d, general_deconv2d
from model import build_resnet_block

from data_provider import provide_custom_data

from absl import logging
logging._warn_preinit_stderr = 0

tfgan = tf.contrib.gan


class CycleWGAN():

    def __init__(self, input_folder, nrb=9, ngf=32, ndf=64, pool_size=50,
                 gp_weight=10, cyc_weight=10, batch_size=8, lr=2e-4,
                 num_steps=100000):
        self.domain_A_train = glob.glob(input_folder + '/trainA/*.jpg')
        self.domain_B_train = glob.glob(input_folder + '/trainB/*.jpg')
        self.domain_A_test = glob.glob(input_folder + '/testA/*.jpg')
        self.domain_B_test = glob.glob(input_folder + '/testB/*.jpg')

        self.nrb = nrb  # number of resnet blocks
        self.ngf = ngf  # number of generator filter
        self.ndf = ndf  # number of discriminator filter
        self.pool_size = pool_size  # number of fake images for comparisaon
        self.gp_weight = gp_weight  # gradient penalty weight
        self.cyc_weight = cyc_weight  # weight of cycle cycle_consistency_loss
        self.batch_size = batch_size
        self.img_layer = int(3)
        self.img_height = 256
        self.img_width = 256
        self.img_size = self.img_width * self.img_height
        self.lr = lr
        self.num_steps = num_steps

        self.cyclegan_model = None
        self.cyclegan_loss = None

        self.to_train = True
        self.to_test = False
        self.log_dir = "./output/" + time.strftime("%Y_%m_%d_%H_%M_%S")

        print('start reading...')
        self.data_x, self.data_y = provide_custom_data([self.domain_A_train,
                                                        self.domain_B_train],
                                                       self.batch_size,
                                                       patch_size=256)
        print('done reading')
        self.data_x.set_shape([self.batch_size, None, None, None])
        self.data_y.set_shape([self.batch_size, None, None, None])

    def generator_network(self, gen_input):

        f = 7
        ks = 3
        print('TYPE', type(gen_input), gen_input)

        pad_input = tf.pad(gen_input, [[0, 0], [ks, ks], [ks, ks], [0, 0]], "REFLECT")
        o_c1 = general_conv2d(pad_input, self.ngf, f, f, 1, 1, name="c1")
        o_c2 = general_conv2d(o_c1, self.ngf * 2, ks, ks, 2, 2, "SAME", "c2")
        x = general_conv2d(o_c2, self.ngf * 4, ks, ks, 2, 2, "SAME", "c3")

        for i in range(self.nrb):
            x = build_resnet_block(x, self.ngf*4, 'r'+str(i+1))

        o_c4 = general_deconv2d(x, [self.batch_size, 64, 64, self.ngf * 2], self.ngf * 2, ks, ks, 2, 2, "SAME", "c4")
        o_c5 = general_deconv2d(o_c4, [self.batch_size, 128, 128, self.ngf], self.ngf, ks, ks, 2, 2, "SAME", "c5")
        o_c5_pad = tf.pad(o_c5, [[0, 0], [ks, ks], [ks, ks], [0, 0]], "REFLECT")
        o_c6 = general_conv2d(o_c5_pad, self.img_layer, f, f, 1, 1, "VALID", "c6", do_relu=False)

        # Adding the tanh layer

        out_gen = tf.nn.tanh(o_c6, "t1")

        return out_gen

    def discriminator_network(self, disc_input, _):

        f = 4

        o_c1 = general_conv2d(disc_input, self.ndf, f, f, 2, 2, "SAME", "c1", do_norm=False, relufactor=0.2)
        o_c2 = general_conv2d(o_c1, self.ndf * 2, f, f, 2, 2, "SAME", "c2", do_norm=False, relufactor=0.2)
        o_c3 = general_conv2d(o_c2, self.ndf * 4, f, f, 2, 2, "SAME", "c3", do_norm=False, relufactor=0.2)
        o_c4 = general_conv2d(o_c3, self.ndf * 8, f, f, 1, 1, "SAME", "c4", do_norm=False, relufactor=0.2)
        o_c5 = general_conv2d(o_c4, 1, f, f, 1, 1, "SAME", "c5", do_norm=False, do_relu=False)

        return o_c5

    def generator_loss(self, model, add_summaries):
        loss = tfgan.losses.wasserstein_generator_loss(model, add_summaries=add_summaries)
        # cyc_loss = self.cyc_weight * tfgan.losses.cycle_consistency_loss(model, add_summary=False)

        # tf.summary.scalar('generator_loss', loss)
        # tf.summary.scalar('cycle_consistency_loss', cyc_loss)

        return loss  # + cyc_loss

    def discriminator_loss(self, model, add_summaries):
        loss = tfgan.losses.wasserstein_discriminator_loss(model, add_summaries=add_summaries)
        gp_loss = self.gp_weight * tfgan.losses.wasserstein_gradient_penalty(model, add_summaries=add_summaries)

        # tf.summary.scalar('discriminator_loss', loss)
        # tf.summary.scalar('gradient_penalty_loss', gp_loss)

        return loss + gp_loss

    def build(self):
        print('build')
        cycleWGAN_model = tfgan.cyclegan_model(
            generator_fn=self.generator_network,
            discriminator_fn=self.discriminator_network,
            data_x=self.data_x,
            data_y=self.data_y
        )
        print('model was build')
        tfgan.eval.add_cyclegan_image_summaries(cycleWGAN_model)

        cycleWGAN_loss = tfgan.cyclegan_loss(
            cycleWGAN_model,
            generator_loss_fn=self.generator_loss,
            discriminator_loss_fn=self.discriminator_loss,
            cycle_consistency_loss_fn=tfgan.losses.cycle_consistency_loss,
            cycle_consistency_loss_weight=self.cyc_weight,
            tensor_pool_fn=tfgan.features.tensor_pool
        )
        print('loss model')
        gen_opt = tf.compat.v1.train.AdamOptimizer(self.lr,0.5,0.9)
        disc_opt = tf.compat.v1.train.AdamOptimizer(self.lr,0.5,0.9)

        gan_train_ops = tfgan.gan_train_ops(
            cycleWGAN_model,
            cycleWGAN_loss,
            gen_opt,
            disc_opt
        )
        print('trainpos')
        status_message = tf.strings.join(
            ['Starting train step: ',
             tf.as_string(tf.compat.v1.train.get_or_create_global_step())
            ], name='status_message')

        hooks_fn = tfgan.get_sequential_train_hooks(tfgan.GANTrainSteps(1, 5))
        hooks = [tf.estimator.StopAtStepHook(num_steps=self.num_steps),
                 tf.estimator.LoggingTensorHook([status_message], every_n_iter=1000)]
        if not os.path.exists(self.log_dir):
            os.makedirs(self.log_dir)

        self.gan_train = tfgan.gan_train(
            train_ops=gan_train_ops,
            logdir=self.log_dir,
            get_hooks_fn=hooks_fn,
            hooks=hooks,
            save_checkpoint_secs=600,
            save_summaries_steps=100,
        )


print('start')
dataset = "./datasets/horse2zebra"
model = CycleWGAN(dataset)
print('build')
model.build()
print('success')
